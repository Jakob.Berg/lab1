package INF101.lab1.rockPaperScissors;

import java.util.List;
import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;


public class RockPaperScissors {
	
    public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        RockPaperScissors game = new RockPaperScissors();
        game.run();
        
        
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        System.out.println("Let's play round " +roundCounter);
        String human = intro();
        winner(human);
        System.out.println("Score: human " + humanScore + ", computer " + computerScore);
        roundCounter += 1;
        
        while(true){
            String continuePlaying = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (continuePlaying.equals("y")){
            run();
            break;
            }
            else if (continuePlaying.equals("n")) {
                System.out.println("Bye bye :)");
                closeScanner();
                break;
            
            
            }
            else {
            System.out.println("I do not understand");
            roundCounter -= 1;
            
        }
        }
    }
    

    public void winner(String human){
        Random random = new Random(); 
        int randomIndex = random.nextInt(rpsChoices.size());
        String computer = rpsChoices.get(randomIndex).toLowerCase();
        if (human.equals(computer)){
            System.out.println("Human chose "+ human + " computer chose " + computer + " it's a tie!  ");}

        else if (human.equals("rock")&&(computer.equals("paper"))){
            System.out.println("Human chose "+ human + " computer chose " + computer + " computer wins!");
            computerScore +=1;}

        else if (human.equals("paper")&&(computer.equals("rock"))){
            System.out.println("Human chose "+ human + " computer chose " + computer + " human wins!");
            humanScore +=1;}

        else if (human.equals("scissors")&&(computer.equals("paper"))){
            System.out.println("Human chose "+ human + " computer chose " + computer + " human wins!");
            humanScore +=1;}

        else if (human.equals("paper")&&(computer.equals("scissors"))){
            System.out.println("Human chose "+ human + " computer chose " + computer + " computer wins!");
            computerScore +=1;}

        else if (human.equals("rock")&&(computer.equals("scissors"))){
            System.out.println("Human chose "+ human + " computer chose " + computer + " human wins!");
            humanScore +=1;}
                
        else if (human.equals("scissors")&&(computer.equals("rock"))){
            System.out.println("Human chose "+ human + " computer chose " + computer + " computer wins!");
            computerScore +=1;}
        
        }

    
    public String intro(){
        String human = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
        if ((!human.equals("rock")) && 
            (!human.equals("paper")) &&
            (!human.equals("scissors"))){
            System.out.println("I do not understand " + human + ". Could you try again?");
            return intro();
            }
        return human;}
        
    
    
            
    public void closeScanner() {
        sc.close();  
    }
            
        
          
    
    
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.nextLine();
        return userInput;
    }

}