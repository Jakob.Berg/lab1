package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        int numRows = grid.size();
        int numCols = grid.get(0).size();

        int referansesum = 0;
        for (int j = 0; j<numRows; j++){
            referansesum += grid.get(0).get(j);
        }
        for (int i = 0; i<numRows; i++){
            int rowSum = 0;
            for (int l = 0; l<numCols; l++){
                rowSum += grid.get(i).get(l);
            }
            if (rowSum != referansesum){
                return false;
            }
    }
        for (int i = 0; i < numCols; i++){
            int colSum = 0;
            for (int j = 0; j<numRows; j++){
                colSum += grid.get(j).get(i);
            }
            if (colSum != referansesum){
                return false;
        }

        }
        return true;
    }
}
    
        
        
            

                 
            
        

        
    

